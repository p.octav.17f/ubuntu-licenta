-- MySQL dump 10.13  Distrib 8.0.20, for Linux (x86_64)
--
-- Host: localhost    Database: planetickets
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `availableseats`
--

DROP TABLE IF EXISTS `availableseats`;
/*!50001 DROP VIEW IF EXISTS `availableseats`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `availableseats` AS SELECT 
 1 AS `ID`,
 1 AS `total seats`,
 1 AS `free seats`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `flights`
--

DROP TABLE IF EXISTS `flights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flights` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `departure location` varchar(30) NOT NULL DEFAULT ',',
  `departure date` datetime NOT NULL,
  `arrival location` varchar(30) NOT NULL DEFAULT ',',
  `flight duration` time NOT NULL,
  `travel class` varchar(30) NOT NULL DEFAULT 'economy',
  `cabin luggage` varchar(30) NOT NULL DEFAULT 'NO',
  `hold luggage` varchar(30) NOT NULL DEFAULT 'NO',
  `seats` int NOT NULL DEFAULT '5',
  `image path` varchar(30) NOT NULL,
  `price` int NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flights`
--

LOCK TABLES `flights` WRITE;
/*!40000 ALTER TABLE `flights` DISABLE KEYS */;
INSERT INTO `flights` VALUES (93,'Cusco,Peru','2017-02-23 07:40:05','Vitoria,Brazil','12:41:29','business','NO','NO',9,'../images/i12.jpg',297),(94,'Ljubljana,Slovenia','2016-07-22 19:33:13','Hargeisa,Somalia','02:41:19','first','5','60',8,'../images/i21.jpg',171),(95,'Hagatna,United States','2017-04-13 19:53:10','Havana,Cuba','01:29:49','economy','NO','NO',3,'../images/i10.jpg',505),(96,'Puerto Ayacucho,Venezuela','2019-05-16 21:13:05','Enugu,Nigeria','15:48:04','first','NO','NO',7,'../images/i35.jpg',245),(97,'Chandigarh,India','2018-05-01 03:02:54','Bordeaux,France','07:14:22','business','NO','NO',5,'../images/i18.jpg',341),(98,'Dunedin,New Zealand','2018-11-30 07:24:12','Rochester,United States','09:25:04','business','NO','85',3,'../images/i25.jpg',210),(99,'Tunis,Tunisia','2018-03-05 17:03:37','Nassau,Bahamas','08:05:54','first','NO','38',9,'../images/i0.jpg',432),(100,'Cockburn Town,United Kingdom','2017-06-01 06:24:21','Strasbourg,France','21:49:05','first','NO','NO',11,'../images/i10.jpg',268),(101,'Paramaribo,Suriname','2018-04-23 16:25:31','Des Moines,United States','12:25:42','first','NO','62',10,'../images/i18.jpg',506),(102,'Port Moresby,Papua New Guinea','2018-06-07 23:32:02','Anchorage,United States','16:52:54','first','NO','94',4,'../images/i9.jpg',341),(103,'Porto,Portugal','2017-12-16 09:18:20','Fukushima,Japan','21:01:51','economy','NO','38',3,'../images/i35.jpg',461),(104,'Hermosillo,Mexico','2020-05-05 12:45:57','N`Djamena,Chad','02:28:12','business','NO','32',4,'../images/i9.jpg',313),(105,'Toyama,Japan','2020-08-09 10:30:54','Lviv,Ukraine','01:38:00','first','11','71',1,'../images/i27.jpg',376),(106,'Yellowknife,Canada','2018-12-16 04:29:04','Takamatsu,Japan','21:09:19','business','NO','NO',8,'../images/i28.jpg',405),(107,'Makati,Philippines','2019-10-29 04:36:01','Kandahar,Afghanistan','11:20:54','business','NO','44',9,'../images/i16.jpg',332),(108,'Warsaw,Poland','2020-02-05 21:16:42','Palmas,Brazil','13:06:35','economy','NO','NO',1,'../images/i11.jpg',488),(109,'Horta,Portugal','2016-05-10 19:55:34','Mata-Utu,France','03:16:00','first','NO','NO',4,'../images/i24.jpg',324),(110,'Helena,United States','2020-06-26 10:00:48','The Valley,United Kingdom','10:02:40','first','NO','NO',5,'../images/i15.jpg',281),(111,'New York City,United States','2018-06-22 12:09:18','Izhevsk,Russia','07:26:56','first','NO','NO',3,'../images/i10.jpg',265),(112,'Mumbai,India','2018-04-11 23:31:34','San Sebastian,Spain','15:04:03','first','NO','NO',2,'../images/i8.jpg',248),(113,'Cluj-Napoca,Romania','2018-11-05 16:08:25','Nelspruit,South Africa','00:17:25','business','NO','NO',8,'../images/i6.jpg',278),(114,'Adak,United States','2016-07-06 10:57:20','Hannover,Germany','17:20:57','business','NO','34',5,'../images/i27.jpg',344),(115,'Yangon,Burma','2019-08-25 08:51:32','Tiksi,Russia','03:40:31','business','NO','33',11,'../images/i29.jpg',296),(116,'Kofu,Japan','2017-08-08 00:04:22','Andorra la Vella,Andorra','03:18:21','first','13','60',10,'../images/i34.jpg',188),(117,'Kochi,India','2019-04-09 18:47:52','Virginia Beach,United States','22:36:45','business','NO','85',5,'../images/i29.jpg',234),(118,'Livingstone,Zambia','2019-08-14 22:51:36','Kolkata (Calcutta),India','03:58:23','economy','NO','NO',10,'../images/i22.jpg',306),(119,'Tromso,Norway','2018-10-17 20:44:30','Mersin,Turkey','12:54:13','business','NO','99',3,'../images/i5.jpg',368),(120,'Udon Thani,Thailand','2017-07-15 11:43:25','Puerto Aisen,Chile','07:09:37','first','NO','NO',4,'../images/i25.jpg',493),(121,'Colombo,Sri Lanka','2016-05-07 08:56:30','Paramaribo,Suriname','00:07:00','economy','NO','NO',3,'../images/i0.jpg',241),(122,'Hamilton,United Kingdom','2018-08-12 07:17:34','Salta,Argentina','09:34:13','economy','NO','72',1,'../images/i3.jpg',454);
/*!40000 ALTER TABLE `flights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logindata`
--

DROP TABLE IF EXISTS `logindata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `logindata` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(30) NOT NULL,
  `first name` varchar(30) NOT NULL,
  `last name` varchar(30) NOT NULL,
  `adress` varchar(30) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `coco` int NOT NULL DEFAULT '200',
  `user type` varchar(10) NOT NULL DEFAULT 'user',
  `validation status` varchar(10) NOT NULL DEFAULT 'valid',
  `connection status` varchar(10) NOT NULL DEFAULT 'offline',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1012 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logindata`
--

LOCK TABLES `logindata` WRITE;
/*!40000 ALTER TABLE `logindata` DISABLE KEYS */;
INSERT INTO `logindata` VALUES (60,'petreocty1998','cartof123','amigo_octy@gmail.com','octavian','petre','amigo','0741',703,'user','valid','offline'),(63,'admin','admin','amadsadsaigo@gmail.com','q','d','asd','w2',98605,'admin','valid','online'),(65,'qaz','qaz','qaz','qaz','qaz','qaz','qaz',1000,'user','valid','offline'),(999,'','','','','',NULL,NULL,0,'','',''),(1009,'user123','pwas123','kilimajaro@gmail.com','qaz','wsx','Cal. bucuresti nr.38','03125846',200,'user','valid','offline');
/*!40000 ALTER TABLE `logindata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `ID flight` int NOT NULL,
  `ID user` int NOT NULL,
  `taken seats` int NOT NULL,
  `status` varchar(30) NOT NULL DEFAULT 'unhonored',
  PRIMARY KEY (`ID`),
  KEY `flight_key` (`ID flight`),
  KEY `user_key` (`ID user`),
  CONSTRAINT `flight_key` FOREIGN KEY (`ID flight`) REFERENCES `flights` (`ID`),
  CONSTRAINT `user_key` FOREIGN KEY (`ID user`) REFERENCES `logindata` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transactions` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `time` datetime NOT NULL,
  `money` int NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES (1,'0','admin','2019-05-18 23:34:10',171),(2,'0','admin','2019-05-18 23:36:13',0),(3,'0','admin','2019-05-18 23:36:28',0),(4,'0','admin','2019-05-19 00:41:24',297),(5,'2','admin','2019-05-19 00:45:32',297),(6,'0','admin','2019-05-19 09:28:21',297),(7,'0','admin','2019-05-19 09:33:54',594),(8,'0','admin','2019-05-19 10:12:27',2970),(9,'0','admin','2019-05-19 10:52:41',1485),(10,'0','admin','2019-05-19 12:07:48',2673),(11,'UR8r16uj','admin','2019-05-19 12:32:21',1485),(12,'BXgTSAY8','admin','2019-05-19 12:32:44',1188),(13,'MudnuVHZ','petreocty1998','2019-05-24 18:54:47',297);
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `availableseats`
--

/*!50001 DROP VIEW IF EXISTS `availableseats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `availableseats` AS select `flights`.`ID` AS `ID`,`flights`.`seats` AS `total seats`,`flights`.`seats` AS `free seats` from `flights` group by `flights`.`ID` order by `flights`.`seats` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-06 16:22:42
