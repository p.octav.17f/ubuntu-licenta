<!DOCTYPE html>
<html>
<head>
	<head>
		<title>Destinations</title>
		<link rel="stylesheet" type="text/css" href="../styles/Destinations.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<script src="../scripts/AllFlights.js"></script>

		<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYyns3ehc1GEsMTJ2r8nVqv51f6pxVnyg&callback=initMap"
		type="text/javascript"></script>
	</head>
</head>
<body>

	<?php  
		include("../sources/Navbar.php");
		include_once '../phps/SQL_Connection.php';
	?>
	
	<div class="content mx-5 my-5 ">
	<?php 
		$sql_show_all_destinations = "SELECT `arrival location`, `image path` from flights";
		$query_show_all_destinations = mysqli_query($conn,$sql_show_all_destinations);
		if(!$query_show_all_destinations){
                        die('Invalid query: ' . mysql_error());
			exit("Error at selecting all destination");
		}

		while ($row = mysqli_fetch_assoc($query_show_all_destinations)) {
			$arriv_loc = explode(',',$row['arrival location'],2);
			echo 
			'<div class="gallery">

				<div >
					<img src="' . $row['image path'] . '"  width="600" height="400">
				</div>
				<div class="desc"><p>' . $arriv_loc[0] . '</p><hr><p>' . $arriv_loc[1] . '</p></div>
				
				<form action="../phps/Booking.php?search_loc=' . $arriv_loc[1] . '" method="POST">
					<button type="submit" class="btn_search btn  btn-warning btn-sm mb-1" >Search</button>
				</form> 
			</div>';
		}

	?>
	</div>


	
		


	





</body>
</html>
