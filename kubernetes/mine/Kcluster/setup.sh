#!/bin/bash

minikube start --vm-driver=docker --memory 8192

kubectl apply -f scrt_dockerHub.yaml 
#kubectl apply -f clusterRole.yaml
kubectl apply -f fabric8-rbac.yaml
sleep 10
kubectl patch serviceaccount default -p "{\"imagePullSecrets\": [{\"name\": \"dh-secret\"}]}"
kubectl apply -f jenkins.yaml

minikube mount /home/ubuntu/.minikube:/selfconfig 

