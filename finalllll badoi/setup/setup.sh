#!/bin/bash

minikube start --vm-driver=docker --memory 8192

kubectl apply -f secretDockerHub.yaml 
kubectl apply -f fabric8-rbac.yaml
sleep 10
kubectl patch serviceaccount default -p "{\"imagePullSecrets\": [{\"name\": \"dh-secret\"}]}"

minikube mount /home/ubuntu/.minikube:/selfconfig &

sleep 10
kubectl apply -f jenkins.yaml
